package com.app1.app1.service;

import com.app1.app1.DTO.GetDto;
import com.app1.app1.DTO.PostDTO;
import com.app1.app1.DTO.RequestPostDto;
import com.app1.app1.model.Zip;
import com.app1.app1.repository.ZipRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
class ZipServiceTest {

    @Mock
    private ZipRepository zipRepository;

    @Spy
    private ModelMapper modelMapper;

    private ZipService zipService ;

    @BeforeEach
    void setUp() {

        zipService = new ZipService(modelMapper, zipRepository);
    }


    @Test
    void givenIdNotFoundExpectException() {
        int id = 1000;
        assertThatThrownBy(() -> zipService.getZip(id))
                .isInstanceOf(NullPointerException.class).hasMessage(id + "'s Zip File Not Found");
    }

    @Test
    void whenGetZipThenExceptZipInformation() {
        // Arrange
        Zip zip = new Zip();

        zip.setId(1);
        zip.setFilepath("C:\\my_folder\\x");
        zip.setStatus("COMPLETED");
        zip.setZipFilePath(zip.getFilepath() + zipService.returnDateNowToString() + ".zip");

        when(zipRepository.findById(zip.getId())).thenReturn(Optional.of(zip));

        GetDto getDto = new GetDto();

        getDto.setFilepath(zip.getFilepath());
        getDto.setStatus(zip.getStatus());
        getDto.setZipFilePath(zip.getZipFilePath());

        //Act
        assertThat(zipService.getZip(zip.getId())).isEqualTo(getDto);


    }

    @Test
    void whenGivenPathNotFoundExpectException() {

        RequestPostDto requestPostDto = new RequestPostDto();
        requestPostDto.setFilepath("C:/z");

        assertThatThrownBy(() -> zipService.create(requestPostDto))
                .isInstanceOf(RuntimeException.class).hasMessage("This Directory Not Found");

    }

    @Test
    void whenGivenPathExpectedIdOfCreatingZipFile() {
        RequestPostDto requestPostDto = new RequestPostDto();
        requestPostDto.setFilepath("C:\\my_folder\\MyFile.txt");

        PostDTO postDTO = new PostDTO();
        postDTO.setId(5);

        Zip zip = new Zip();
        zip.setId(5);
        zip.setFilepath(requestPostDto.getFilepath());
        zip.setStatus("COMPLETED");
        zip.setZipFilePath(requestPostDto.getFilepath() + zipService.returnDateNowToString() + ".zip");

        when(
                zipRepository.findByFilepathAndStatusAndZipFilePath(
                        requestPostDto.getFilepath(),
                        "COMPLETED",
                        requestPostDto.getFilepath() + zipService.returnDateNowToString() + ".zip"))
                .thenReturn(Optional.of(zip));

        assertThat(zipService.create(requestPostDto)).isEqualTo(postDTO);
    }

    @Test
    void whenGivenPathExpectedIdOfCreatingZipDirectory(){
        RequestPostDto requestPostDto = new RequestPostDto();
        requestPostDto.setFilepath("C:\\my_folder\\x");

        PostDTO postDTO = new PostDTO();
        postDTO.setId(5);

        Zip zip = new Zip();
        zip.setId(5);
        zip.setFilepath(requestPostDto.getFilepath());
        zip.setStatus("COMPLETED");
        zip.setZipFilePath(requestPostDto.getFilepath() + zipService.returnDateNowToString() + ".zip");

        when(
                zipRepository.findByFilepathAndStatusAndZipFilePath(
                        requestPostDto.getFilepath(),
                        "COMPLETED",
                        requestPostDto.getFilepath() + zipService.returnDateNowToString() + ".zip"))
                .thenReturn(Optional.of(zip));

        assertThat(zipService.create(requestPostDto)).isEqualTo(postDTO);
    }
}







