package com.app1.app1.contoller;

import com.app1.app1.DTO.GetDto;
import com.app1.app1.DTO.PostDTO;
import com.app1.app1.DTO.RequestPostDto;
import com.app1.app1.service.ZipService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(ZipController.class)
class ZipControllerTest {

    private static final Integer DUMMY_ID = 1;
    private static final String GET_URL = "/status";
    private static final String POST_URL = "/zip";



    private static final String FILE_PATH = "C:\\my_folder\\MyFile.txt";
    private static final String STATUS = "COMPLETED";
    private static final String ZIP_FILE_PATH = "C:\\my_folder\\MyFile.txt2021-04-30-00-38.zip";


    @MockBean
    private ZipService zipService;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;


    private GetDto getDto;
    private PostDTO postDto;
    private RequestPostDto requestPostDto;


    @BeforeEach
    void setUp() {

        getDto = new GetDto();
        getDto.setFilepath(FILE_PATH);
        getDto.setStatus(STATUS);
        getDto.setZipFilePath(ZIP_FILE_PATH);

        postDto = new PostDTO();
        postDto.setId(DUMMY_ID);

        requestPostDto = new RequestPostDto();
        requestPostDto.setFilepath(FILE_PATH);


    }

    @Test
    void getZipFile() throws Exception {


        when(zipService.getZip(DUMMY_ID)).thenReturn(getDto);

        mockMvc.perform(get(GET_URL + "/" + DUMMY_ID.toString())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.filepath", is(FILE_PATH)))
                .andExpect(jsonPath("$.status", is(STATUS)))
                .andExpect(jsonPath("$.zipFilePath", is(ZIP_FILE_PATH)));

    }






    @Test
    void createZipFile() throws Exception {

        when(zipService.create(requestPostDto)).thenReturn(postDto);

        mockMvc.perform(post(POST_URL)
                .content(objectMapper.writeValueAsString(requestPostDto))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());


    }
}
