package com.app1.app1.service;

import com.app1.app1.DTO.GetDto;
import com.app1.app1.DTO.PostDTO;
import com.app1.app1.DTO.RequestPostDto;
import com.app1.app1.model.Zip;
import com.app1.app1.repository.ZipRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;


@Service
@RequiredArgsConstructor
public class ZipService {

    String dateNow;
    private final ModelMapper mapper;
    private final ZipRepository zipRepository;
    List<String> fileList = new ArrayList<>();

    public GetDto getZip(Integer id) {

        return zipRepository.findById(id)
                .map(zip -> mapper.map(zip, GetDto.class))
                .orElseThrow(() -> new NullPointerException(id + "'s Zip File Not Found"));

    }

    public PostDTO create(RequestPostDto requestPostDto) {

        File file = new File(requestPostDto.getFilepath());
        String fileName = returnToFileName(requestPostDto.getFilepath());

        if (file.isFile()) {

            createFromFileToZip(requestPostDto.getFilepath() + returnDateNowToString() + ".zip", requestPostDto.getFilepath(), fileName);
            dateNow = returnDateNowToString();

            if (new File(requestPostDto.getFilepath() + dateNow + ".zip").exists()) {
                return takePathAndCheckDatabaseCreatingAndReturnPostDto(requestPostDto.getFilepath(), requestPostDto.getFilepath() + dateNow + ".zip");
            }

        } else if (file.isDirectory()) {

            createFromDirectoryToZip(requestPostDto.getFilepath() + returnDateNowToString() + ".zip", requestPostDto.getFilepath());
            dateNow = returnDateNowToString();

            if (new File(requestPostDto.getFilepath() + dateNow + ".zip").exists()) {
                return takePathAndCheckDatabaseCreatingAndReturnPostDto(requestPostDto.getFilepath(), requestPostDto.getFilepath() + dateNow + ".zip");
            }

        }
        throw new RuntimeException("This Directory Not Found");
    }

    public String returnToFileName(String path) {
        String[] arrOfStr = path.split("/", 100);
        return arrOfStr[arrOfStr.length - 1];
    }

    public String returnDateNowToString() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH-mm");
        LocalDateTime now = LocalDateTime.now();
        String nowDate = dtf.format(now).toString();
        return nowDate;
    }


    private PostDTO takePathAndCheckDatabaseCreatingAndReturnPostDto(String path, String zipPath) {
        Zip z = new Zip();
        z.setFilepath(path);
        z.setStatus("COMPLETED");
        z.setZipFilePath(path + dateNow + ".zip");
        zipRepository.save(z);
        return zipRepository.findByFilepathAndStatusAndZipFilePath(path, z.getStatus(), zipPath)
                .map(zip -> mapper.map(zip, PostDTO.class))
                .orElseThrow(() -> new RuntimeException("Zip file path NotFound"));

    }

    private void createFromFileToZip(String zipFilePath, String givenPath, String innerFilename) {
        try (FileOutputStream fos = new FileOutputStream(zipFilePath);
             ZipOutputStream zos = new ZipOutputStream(fos);
             FileInputStream in = new FileInputStream(givenPath);) {
            ZipEntry ze = new ZipEntry(innerFilename);
            zos.putNextEntry(ze);
            byte[] buffer = new byte[1024];
            int len;
            while ((len = in.read(buffer)) > 0) {
                zos.write(buffer, 0, len);
            }

        } catch (IOException e) {
              e.printStackTrace();

        }

    }


    private void createFromDirectoryToZip(String zipFilePath, String path) {
        generateFileList(new File(path),path);
        zipIt(zipFilePath,path);
    }



    private void zipIt(String zipFile,String SOURCE_FOLDER){
        byte[] buffer = new byte[1024];
        try{
            FileOutputStream fos = new FileOutputStream(zipFile);
            ZipOutputStream zos = new ZipOutputStream(fos);
            System.out.println("Output to Zip : " + zipFile);
            for(String file : this.fileList){
                System.out.println("File Added : " + file);
                ZipEntry ze= new ZipEntry(file);
                zos.putNextEntry(ze);
                FileInputStream in =
                        new FileInputStream(SOURCE_FOLDER + File.separator + file);
                int len;
                while ((len = in.read(buffer)) > 0) {
                    zos.write(buffer, 0, len);
                }
                in.close();
            }
            zos.closeEntry();
            //remember close it
            zos.close();
            System.out.println("Done");
        }catch(IOException ex){

        }
    }

    private void generateFileList(File node ,String path){

        if(node.isFile()){
            fileList.add(generateZipEntry(node.getAbsoluteFile().toString(),path));
        }
        if(node.isDirectory()){
            String[] subNote = node.list();
            for(String filename : subNote){
                generateFileList(new File(node, filename),path);
            }
        }
    }

    private String generateZipEntry(String file ,String SOURCE_FOLDER) {
        return file.substring(SOURCE_FOLDER.length() + 1, file.length());
    }


}


