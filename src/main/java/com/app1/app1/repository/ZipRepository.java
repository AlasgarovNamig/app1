package com.app1.app1.repository;

import com.app1.app1.model.Zip;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ZipRepository extends JpaRepository<Zip,Integer> {

    Optional<Zip> findByFilepathAndStatusAndZipFilePath(String filePath ,String status,String zipFilePath);
}
