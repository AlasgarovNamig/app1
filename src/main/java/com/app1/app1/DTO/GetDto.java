package com.app1.app1.DTO;

import lombok.Data;

@Data
public class GetDto {

    private String filepath;
    private String status;
    private String zipFilePath;

}
