package com.app1.app1.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "zips")
public class Zip {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "file_path")
    private String filepath;

    private String status;

    @Column(name = "zip_file_path")
    private String zipFilePath;
}
