package com.app1.app1.contoller;

import com.app1.app1.DTO.GetDto;
import com.app1.app1.DTO.PostDTO;
import com.app1.app1.DTO.RequestPostDto;
import com.app1.app1.service.ZipService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
public class ZipController {

    private final ZipService zipService;

    @GetMapping("/status/{id}")
    public GetDto getZipFile(@PathVariable("id") Integer id) {
        return zipService.getZip(id);
    }

    @PostMapping("/zip")
    public PostDTO createZipFile(@RequestBody RequestPostDto requestPostDto) {
        return zipService.create(requestPostDto);
    }

}
